%define mod_name LWP-MediaTypes
Name:           perl-%{mod_name}
Version:        6.04
Release:        5
Summary:        Guess media type for a file or a URL
License:        (GPL-1.0-or-later OR Artistic-1.0-Perl) and Public Domain
URL:            https://metacpan.org/release/%{mod_name}
Source0:        https://cpan.metacpan.org/authors/id/O/OA/OALDERS/%{mod_name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  make perl-generators perl-interpreter perl(ExtUtils::MakeMaker) perl(strict) perl(warnings) sed perl(Carp)  perl(Exporter) perl(File::Basename) perl(Scalar::Util) perl(File::Spec) perl(overload) perl(Test::Fatal) perl(Test::More)
Requires:       perl(File::Basename) mailcap

Conflicts:      perl-libwww-perl < 6

%description
This module provides functions for handling media (also known as MIME)
types and encodings. The mapping from file extensions to media types is
defined by the media.types file. If the ~/.media.types file exists it is
used instead. For backwards compatibility we will also look for
~/.mime.types.

%package help
Summary:        Documentation for perl-%{mod_name}

%description help
Documentation for perl-%{mod_name}.

%prep
%autosetup -n %{mod_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%{make_build}

%install
%{make_install}
%{__chmod} -Rf a+rX,u+w,g-w,o-w $RPM_BUILD_ROOT/*

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/*
%exclude %{_libdir}/perl5/perllocal.pod

%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 6.04-5
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Oct 24 2022 yangmingtai <yangmingtai@huawei.com> - 6.04-4
- define mod_name to opitomize the specfile

* Wed Apr 27 2022 renhongxun <renhongxun@h-partners.com> - 6.04-3
- rebuild perl-LWP-MediaTypes

* Sat Jul 25 2020 zhanzhimin <zhanzhimin@huawei.com> - 6.04-1
- 6.04 bump

* Fri Oct 25 2019 Zaiwang Li <lizaiwang1@huawei.com> - 6.02-17
- Init Package.

